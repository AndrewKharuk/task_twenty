﻿namespace TaskTwenty
{
    public interface ISerializationContext
    {
        void SetStrategy(ISerializerStrategy serializer);

        string SerializeShape(IShape shape, string filePath);
    }
}
