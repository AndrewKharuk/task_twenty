﻿using System;
using System.IO;
using System.Text.Json;

namespace TaskTwenty
{
    public class JsonSerializerStrategy : ISerializerStrategy
    {
        public string Serialize(IShape shape, string filePath)
        {
            string allFilePath = $"{filePath}.json";

            using (FileStream stream = new FileStream(allFilePath, FileMode.OpenOrCreate))
            {
                JsonSerializer.SerializeAsync(stream, shape, Type.GetType($"TaskTwenty.{shape.Name}"));   
            }

            return $"{shape.Name} Serialized in XML path - {allFilePath}";
        }
    }
}
