﻿namespace TaskTwenty
{
    public class SerializationContext : ISerializationContext
    {
        ISerializerStrategy _serializer;

        public SerializationContext() { }

        public SerializationContext(ISerializerStrategy serializer)
        {
            _serializer = serializer;
        }

        public string SerializeShape(IShape shape, string filePath)
        {
            return _serializer.Serialize(shape, filePath);
        }

        public void SetStrategy(ISerializerStrategy serializer)
        {
            _serializer = serializer;
        }
    }
}
