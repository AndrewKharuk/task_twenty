﻿namespace TaskTwenty
{
    public interface ISerializerStrategy
    {
        string Serialize(IShape shape, string filePath);
    }
}
