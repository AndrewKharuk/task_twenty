﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace TaskTwenty
{
    public class XMLSerializerStrategy : ISerializerStrategy
    {
        public string Serialize(IShape shape, string filePath)
        {
            XmlSerializer formatter = new XmlSerializer(Type.GetType($"TaskTwenty.{shape.Name}"));
            string allFilePath = $"{filePath}.xml";

            using (FileStream stream = new FileStream(allFilePath, FileMode.OpenOrCreate))
            {
                formatter.Serialize(stream, shape);
            }

            return $"{shape.Name} Serialized in XML path - {allFilePath}";
        }
    }
}
