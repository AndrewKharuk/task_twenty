﻿using System;

namespace TaskTwenty
{
    [Serializable]
    public class Dot : IShape
    {
        public string Name { get; set; }

        public string Collor { get; set; }

        public bool Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}
