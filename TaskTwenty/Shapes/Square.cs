﻿using System;

namespace TaskTwenty
{
    [Serializable]
    public class Square : IShape
    {
        public string Name { get; set; }

        public int Edge { get; set; }

        public bool Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}
