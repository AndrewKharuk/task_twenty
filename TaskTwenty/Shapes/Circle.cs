﻿using System;

namespace TaskTwenty
{
    [Serializable]
    public class Circle : IShape
    {
        public string Name { get; set; }

        public int Radius { get; set; }

        public bool Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}
