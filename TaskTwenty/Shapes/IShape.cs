﻿namespace TaskTwenty
{
    public interface IShape
    {
        string Name { get; set; }
        bool Accept(IVisitor visitor);
    }
}
