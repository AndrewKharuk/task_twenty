﻿using System;

namespace TaskTwenty
{
    class VisitorException : Exception
    {
        public VisitorException(string message)
            : base(message)
        { }
    }
}
