﻿using System;
using System.IO;

namespace TaskTwenty
{
    public class SerializationHalper
    {
        public void CheckOrCreateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

        }

        public string GetFileName(string shapeName)
        {
            return $"{shapeName}_{DateTime.Now.ToShortDateString()}_{GetTime()}";
        }

        private string GetTime()
        {
            var time = DateTime.Now.ToLongTimeString();

            return time.Replace(':', '-'); ;
        }

    }
}
