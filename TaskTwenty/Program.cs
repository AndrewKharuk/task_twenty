﻿using System;

namespace TaskTwenty
{
    class Program
    {
        public static readonly string directoryPath =  @"D:\serializationFiles\";

        static void Main(string[] args)
        {
            IShape shape = new Circle { Name = "Circle", Radius = 111 };
            //IShape shape = new Square { Name = "Square", Edge = 50 };
            //IShape shape = new Dot { Name = "Dot", Collor = "Red" };
            ISerializerStrategy serializer = new XMLSerializerStrategy();
            //ISerializerStrategy serializer = new JsonSerializerStrategy();
            IVisitor visitor = new XMLExportVisitor();
            //IVisitor visitor = new JsonExportVisitor();

            SerializationService service = new SerializationService();

            string filePath = string.Empty;

            try
            {
                filePath = service.ToDo(shape, serializer, visitor, directoryPath);
            }
            catch (VisitorException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine(filePath);

            Console.ReadKey();
        }
    }
}
