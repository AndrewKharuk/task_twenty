﻿namespace TaskTwenty
{
    public class SerializationService
    {
        ISerializationContext context;
        SerializationHalper helper;

        public SerializationService()
        {
            context = new SerializationContext();
            helper = new SerializationHalper();
        }

        public string ToDo(IShape shape, ISerializerStrategy serializer, IVisitor visitor, string directoryPath)
        {
            if (!shape.Accept(visitor))
                throw new VisitorException($"{shape.Name} не обрабатывается выбранным методом.");

            helper.CheckOrCreateDirectory(directoryPath);
            string filePath = $"{directoryPath}{helper.GetFileName(shape.Name)}";

            context.SetStrategy(serializer);
            return context.SerializeShape(shape, filePath);
        }
    }
}
