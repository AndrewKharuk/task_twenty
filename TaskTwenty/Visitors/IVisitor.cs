﻿namespace TaskTwenty
{
    public interface IVisitor
    {
        bool Visit(Circle circle);
        bool Visit(Dot dot);
        bool Visit(Square square);
    }
}
