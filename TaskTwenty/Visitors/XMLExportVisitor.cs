﻿namespace TaskTwenty
{
    public class XMLExportVisitor : IVisitor
    {
        public bool Visit(Circle circle)
        {
            return true;
        }

        public bool Visit(Dot dot)
        {
            return false;
        }

        public bool Visit(Square square)
        {
            return true; 
        }
    }
}
